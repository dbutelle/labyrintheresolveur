/* ----------------------------------------------------------------- 
 * définition des structures nécessaire pour représenter la matrice
 * d'adjacence du graphe de parcours d'un labyrinthe
 * -----------------------------------------------------------------
 */

/*
 * structure d'un maillon de la liste chaînée associé 
 * à chaque ligne de la matrice
 */
struct Maillon {
  int l, c; // coordonnées du noeud dans la grille du labyrinthe
  Maillon *suiv;// élément suivant  sur la ligne
};

/*
 * structure représentant une matrice d'adjacence sous forme
 * de matrice creuse. La structure contient un tableau de lignes, chaque
 * ligne étant représentée par une liste chaînée de "maillons".
 */
struct MatriceAdjacence {
  int ordre; // nombre de sommets du graphe
  Maillon* *lignes; // tableau à allouer de taille "ordre", représentant les lignes de la matrice
};

/* ----------------------------------------------------------------- 
 * définition de la structure de labyrinthe
 * -----------------------------------------------------------------
 */
struct labyrinthe {
  int largeur, hauteur;// nombre de colonnes et de lignes du labyrinthe
  int *cell; // cellules du labyrinthes
  bool *mursV;// état des murs verticaux des cellules
  bool *mursH;// état des murs horizontaux des cellules
};

enum Couleur {BLANC, GRIS, NOIR};

// valeur indéfinie pour un indice de parent
#define INDEFINI  -1

// valeur infinie pour une distance enre sommets
#define INFINI  -1

struct MaillonEntier {
  int valeur; // valeur stockée dans le maillon
  MaillonEntier *suiv;// élément suivant dans la file
  MaillonEntier *prec;// élément précédent dans la file
} ;

/*
 * structure d'une file de type FIFO pouvant contenir des entiers
 */
struct Fifo {
  MaillonEntier *in;
  MaillonEntier *out;
};

void initialiserFifo(Fifo &f){
  f.in = f.out = nullptr;
}

bool estVide(Fifo f){
  return f.out == nullptr;// les deux pointeurs sont forcément nuls
}

    
void ajouter(Fifo &f, int v){
  // création du nouveau maillon
  MaillonEntier *nouv = new MaillonEntier;
  nouv->valeur = v;
  // insertion en tête
  nouv->suiv = f.in;
  nouv->prec = nullptr;
  // maj du pointeur d'entrée
  if(f.in!=nullptr)
    f.in->prec = nouv;
  f.in = nouv;
  // maj du pointeur de sortie
  if(f.out == nullptr)
    f.out = nouv;
}

// cette version suppose la file non vide ...
// la présence d'au moins un élément dans la file
// doit donc être traitée avant l'appel à retirer()
int retirer(Fifo &f){
  // récupérattion de la valeur
  MaillonEntier *cur = f.out;
  int v = cur->valeur;
  // maj des pointeurs d'entrée et de sortie
  f.out = cur->prec;
  if(f.out!=nullptr)// file non vide
    f.out->suiv = nullptr;
  else // file vide après le retrait
    f.in = nullptr;

  return v;
}
/* ----------------------------------------------------------------- 
 * définition de la structure d'une coordonnée et d'un chemin
 * -----------------------------------------------------------------
 */
struct coordonnee {
  int x, y; // abscisse et ordonnée d'une case du labyrinthe
};


struct chemin {
  int lg; // longueur du chemin (nb de cases du tableau etape)
  coordonnee *etape;// les coordonnées des différents sommets à parcourir
};
