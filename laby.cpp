#include <iostream>
#include <fstream>
#include <string>
#include <sstream>
#include <cstdlib>
#include <ctime>
using namespace std;

#include "types.hpp"

#define CELLSIZE 50 // taille d'un côté d'une case du labyrinthe
#define MARGE  10   // marge du dessin
#define TEXTSIZE 10 // taille d'affichage du texte

#define LARGEUR 10  // largeur par défaut du labyrinthe
#define HAUTEUR 10  // hauteur par défaut du labyrinthe

// --------------------------------------------------------------------
// prédéfinition des fonctions liées à la gestion d'un labyrinthe
// --------------------------------------------------------------------
void initialiserLabyrinthe(labyrinthe &laby, int largeur, int hauteur);

void effacerLabyrinthe(labyrinthe &laby);

void genererLabyrinthe(labyrinthe &laby);

void dessinerLabyrinthe(labyrinthe &laby, const string &nomFichier);

// --------------------------------------------------------------------
// prédéfinition des fonctions liées à l'écriture dans les fichiers SVG
// --------------------------------------------------------------------

// création du fichier et ouverture du flot d'écriture associé
// retourne false si le fichier n'a pas pu être ouvert.
bool ouvrirFichierSVG(const string &nomFichier,// nom du fichier à créer
		      ofstream &out,// flot d'écriture ouvert par la fonction
		      int largeur, // largeur de l'image en pixels
		      int hauteur);// hauteur de l'image en pixels

// fermeture du flot d'écriture et du fichier associé
void fermerFichierSVG(ofstream &out);// flot d'écriture à fermer par la fonction

// écriture d'une ligne dans le flot de sortie
void ligne(ofstream &out,// flot d'écriture dans lequel ajouter la ligne
	   int x1, int y1,// coordonnées du point de départ de la ligne
	   int x2, int y2,// coordonnées du point d'arrivée de la ligne
	   const string& color,// couleur de tracé de la ligne
	   int width);// épaisseur de tracé de la ligne

// écriture d'un rectangle dans le flot de sortie
void rect(ofstream &out,// flot d'écriture dans lequel ajouter le rectangle
	  int x, int y,// coordonnées du point supérieur gauche du rectangle
	  int width,// largeur du rectangle en pixels
	  int height,// hauteur du rectangle en pixels
	  const string &color);// couleur de tracé du rectangle

// écriture d'un texte dans le flot de sortie
void text(ofstream &out,// flot d'écriture dans lequel ajouter le texte
	  int x, int y,// coordonnées du point auquel placer le texte
	               // le texte est centré par rapport à ce point
	  int size,// hauteur des caractères du texte en pixels
	  const string &txt,// le texte à ajouter
	  const string &color);// la couleur de tracé du texte

void creerMatrice(MatriceAdjacence &m, int taille){
  // initialisation du nombre de lignes/colonnes de la matrice
  m.ordre = taille;
  // allocation mémoire du tableau de lignes
  m.lignes = new Maillon*[taille];
  // initialisation de chaque ligne à "vide"
  for(int i=0; i<taille; i++) m.lignes[i]=nullptr;
}

void effacerMatrice(MatriceAdjacence &mat){
  for(int l=0; l<mat.ordre; l++){// effacer chaque ligne
    while(mat.lignes[l]!=nullptr){// tq la ligne n'est pas vide
      // effacer le premier élément qui s'y trouve
      Maillon *cour = mat.lignes[l];// 1er élément de la liste
      mat.lignes[l] = cour->suiv;// élément suivant éventuel
      delete cour; // effacer le 1er élement courant
    }
  }
  // effacer le tableau de lignes
  delete mat.lignes;
  // raz de la taille
  mat.ordre = 0;
}

void afficherMatrice(MatriceAdjacence & mat){
  // affichage de chacune des lignes
  for(int l=0; l<mat.ordre; l++){// affichage de la ligne l
	cout<<"noeud "<<l<<" : ";
    Maillon *mcur=mat.lignes[l];
    while(mcur){
		cout<<"("<<mcur->l<<" , "<<mcur->c<<") ";
		mcur = mcur->suiv;
    }// while
    cout << endl;// fin de la ligne l
  }// for
}

void remplirMatrice(MatriceAdjacence & mat, const labyrinthe & laby){
	for(int i = 0; i < mat.ordre; i++){
		Maillon *fin = nullptr;
		for(int j = 0; j < mat.ordre; j++){
			bool mur = true;
			if(j == (i + 1)){
				mur = laby.mursV[j]; //mur de droite
			 } else if(j == (i + laby.largeur)){
				mur = laby.mursH[j];//mur du bas
			 } else if(j == (i - 1)){
				mur = laby.mursV[i];//mur de gauche
			} else if(j == (i - laby.largeur)){
				mur = laby.mursH[i];//mur du haut
			}
			
			if(!mur){
				Maillon *cell = new Maillon;
				cell->l = j/laby.largeur;
				cell->c = j%laby.largeur;
				cell->suiv = nullptr;
				if(fin ==nullptr){
					mat.lignes[i] = cell;
				}else{
					fin->suiv = cell;
				}
				fin = cell;
			}
		}
	} 
}

void dessinerMatrice(const MatriceAdjacence &mat, const labyrinthe &laby, const string &nom){
	ofstream out;

  if(!ouvrirFichierSVG(nom, out,
		       laby.largeur*CELLSIZE+2*MARGE,
		       laby.hauteur*CELLSIZE+2*MARGE))
    return;

  // dessin du fond
  rect(out, 0, 0,
       laby.largeur*CELLSIZE+2*MARGE,
       laby.hauteur*CELLSIZE+2*MARGE, "white");

   // dessin des murs verticaux
  for(int c=0; c<laby.largeur+1; c++)
    for(int l=0; l<laby.hauteur; l++)
      if(laby.mursV[c+l*laby.largeur]){// dessiner un mur vertical
	ligne(out,
	      c*CELLSIZE+MARGE, l*CELLSIZE+MARGE,
	      c*CELLSIZE+MARGE, (l+1)*CELLSIZE+MARGE,
	      "black", 3);
      }
  
  // dessin des murs horizontaux
  for(int c=0; c<laby.largeur; c++)
    for(int l=0; l<laby.hauteur+1; l++)
      if(laby.mursH[c+l*laby.largeur]){// dessiner un mur horizontal
	ligne(out,
	      c*CELLSIZE+MARGE, l*CELLSIZE+MARGE,
	      (c+1)*CELLSIZE+MARGE, l*CELLSIZE+MARGE,
	      "black", 3);
      }
   for(int i = 0; i < mat.ordre; i++){
		Maillon *mcur=mat.lignes[i];
		while(mcur){
			if((mcur->c + mcur->l * laby.largeur) > i){
				ligne(out, (i%laby.largeur + 0.5)*CELLSIZE + MARGE, (i/laby.largeur + 0.5)*CELLSIZE + MARGE, (mcur->c + 0.5)*CELLSIZE + MARGE, (mcur->l + 0.5)*CELLSIZE + MARGE, "green", 1);
			}
			mcur = mcur->suiv;
		}
   }

  fermerFichierSVG(out);
}

void saisirCoordonnees(coordonnee &deb, coordonnee &fin, int largeur, int hauteur){
	int x, y;
	do{
		cout<<"Entrez les coordonnees de la case de depart separees par un espace"<<endl;
		cin>>x;
		cin>>y;
	} while ((x > largeur || x < 0) || (y > hauteur || y < 0));
	deb.x = x;
	deb.y = y;
	
	do{
		cout<<"Entrez les coordonnees de la case de fin separees par un espace"<<endl;
		cin>>x;
		cin>>y;
	} while ((x >= largeur || x < 0) || (y >= hauteur || y < 0));
	fin.x = x;
	fin.y = y;
}

chemin calculerChemin(const MatriceAdjacence &mat, coordonnee deb, coordonnee fin, int largeur){
	chemin parcours;
	parcours.lg = 0;
	int *distances = new int[mat.ordre];
	int *parents = new int[mat.ordre];
	Couleur *couleurs = new Couleur[mat.ordre];
	int caseDep = deb.x + largeur * deb.y;
	int caseFin = fin.x + largeur * fin.y;
	
	for(int i=0; i<mat.ordre; i++){
		couleurs[i] = BLANC;
		distances[i] = INFINI;
		parents[i] = INDEFINI;
	  }

	  distances[caseDep] = 0;
	  couleurs[caseDep] = GRIS;
	  
	  Fifo mafile;
	  initialiserFifo(mafile);
	  ajouter(mafile, caseDep);
	  
	  while(!estVide(mafile)){
		int s = retirer(mafile);
		Maillon *cur = mat.lignes[s];
		while(cur){// pour les sommets adjacents au sommet s
		  int v = cur->c + cur->l*largeur;
		  if(couleurs[v] == BLANC){
		couleurs[v] = GRIS;
		distances[v] = distances[s]+1;
		parents[v] = s;
		ajouter(mafile, v);
		  }
		  cur = cur->suiv;
		}
		couleurs[s] = NOIR;
	  }
	
	parcours.lg = distances[caseFin] + 1;
	
	parcours.etape = new coordonnee[parcours.lg];
	parcours.etape[parcours.lg-1] = fin;
	for(int i = parcours.lg - 1; i > 0; i--){
		coordonnee cell;
		int parent = parcours.etape[i].x + parcours.etape[i].y * largeur;
		cell.x = parents[parent]%largeur;
		cell.y = parents[parent]/largeur;
		parcours.etape[i-1] = cell;
	}
	
	return parcours;
}

void dessinerSolution(const labyrinthe &laby, const chemin &ch, const string &nomFichier){
	ofstream out;

  if(!ouvrirFichierSVG(nomFichier, out,
		       laby.largeur*CELLSIZE+2*MARGE,
		       laby.hauteur*CELLSIZE+2*MARGE))
    return;

  // dessin du fond
  rect(out, 0, 0,
       laby.largeur*CELLSIZE+2*MARGE,
       laby.hauteur*CELLSIZE+2*MARGE, "white");

   // dessin des murs verticaux
  for(int c=0; c<laby.largeur+1; c++)
    for(int l=0; l<laby.hauteur; l++)
      if(laby.mursV[c+l*laby.largeur]){// dessiner un mur vertical
	ligne(out,
	      c*CELLSIZE+MARGE, l*CELLSIZE+MARGE,
	      c*CELLSIZE+MARGE, (l+1)*CELLSIZE+MARGE,
	      "black", 3);
      }
  
  // dessin des murs horizontaux
  for(int c=0; c<laby.largeur; c++)
    for(int l=0; l<laby.hauteur+1; l++)
      if(laby.mursH[c+l*laby.largeur]){// dessiner un mur horizontal
	ligne(out,
	      c*CELLSIZE+MARGE, l*CELLSIZE+MARGE,
	      (c+1)*CELLSIZE+MARGE, l*CELLSIZE+MARGE,
	      "black", 3);
      }
   for(int i = 1; i < ch.lg; i++){
		ligne(out, (ch.etape[i-1].x + 0.5)*CELLSIZE + MARGE, (ch.etape[i-1].y + 0.5)*CELLSIZE + MARGE, (ch.etape[i].x + 0.5)*CELLSIZE + MARGE, (ch.etape[i].y + 0.5)*CELLSIZE + MARGE, "red", 1);
   }

  fermerFichierSVG(out);
}

// --------------------------------------------------------------------
// prédéfinition de la fonction de conversion d'une valeur entière
// en chaîne de caractères
// --------------------------------------------------------------------

string intToString(int v);


// --------------------------------------------------------------------
// programme principal
// --------------------------------------------------------------------

int main(int argc, char *argv[]){
  // analyse des paramètres pour déterminer la taille du labyrinthe
  int largeur, hauteur;
  srand(time(NULL));
  
  if(argc==1){
    // dimensions par défaut
    largeur = LARGEUR;
    hauteur = HAUTEUR;
  } else if(argc==3){
    // lecture des dimensions dans les paramètres
    largeur = atoi(argv[1]);
    hauteur = atoi(argv[2]);
  } else {
    cerr << "syntaxe : " << argv[0] << " [largeur hauteur]" << endl;
    return 0;
  }
  
  // création du labyrinthe
  labyrinthe laby;
  MatriceAdjacence mat;
  chemin ch;
  coordonnee deb, fin;
  
  initialiserLabyrinthe(laby, largeur, hauteur);
  creerMatrice(mat, largeur*hauteur);
  // génération du labyrinthe
  genererLabyrinthe(laby);
  remplirMatrice(mat, laby);

  // dessin du labyrinthe
  dessinerLabyrinthe(laby,"laby.svg");
  
  afficherMatrice(mat);
  dessinerMatrice(mat,laby,"graphe.svg");
  
  saisirCoordonnees(deb,fin,largeur, hauteur);
  ch = calculerChemin(mat, deb, fin, largeur);
  dessinerSolution(laby,ch,"solution.svg");

  // suppression du labyrinthe
  effacerLabyrinthe(laby);
  effacerMatrice(mat);

  return 0;
}

// --------------------------------------------------------------------
// définition des fonctions liées à la gestion d'un labyrinthe
// --------------------------------------------------------------------

void initialiserLabyrinthe(labyrinthe &laby, int largeur, int hauteur){
  // initialisation des dimensions
  laby.largeur = largeur;
  laby.hauteur = hauteur;
  // allocation des zones mémoire
  laby.cell = new int[largeur*hauteur];
  laby.mursV = new bool[(largeur+1)*hauteur];
  laby.mursH = new bool[largeur*(hauteur+1)];
  // initialisation des zones mémoire
  // un identifiant différent par cellule
  for(int i=0; i<largeur*hauteur; i++) laby.cell[i] = i;
  // tous les murs verticaux sont présents
  for(int i=0; i<(largeur+1)*hauteur; i++) laby.mursV[i] = true;
  // tous les murs horizontaux sont présents
  for(int i=0; i<largeur*(hauteur+1); i++) laby.mursH[i] = true;
}

void effacerLabyrinthe(labyrinthe &laby){
  delete [] laby.cell;
  delete [] laby.mursV;
  delete [] laby.mursH;
}

void genererLabyrinthe(labyrinthe &laby){
  // calcul du nombre de murs à supprimer
  int nbm = laby.largeur*laby.hauteur-1;
  int nb=0;

  while(nb<nbm){
    // génération aléatoire d'une case de la grille
    // et d'une direction de suppression
    int c=rand()%laby.largeur;// dans [0,M-1]
    int l=rand()%laby.hauteur;// dans [0,N-1]
    int dir=rand()%4; // 0=droite, 1=haut, 2=gauche, 3=bas
    int oid, nid;// valeur de l'id (oid) à changer en nouvel id (nid)
    bool trouve;

    trouve=false;// on va vérifier que le mur peut être supprimé
    
    switch(dir){
    case 0:// suppression d'un mur droit
      if(c==laby.largeur-1) break;// colonne invalide
      if(!laby.mursV[(c+1)+l*laby.largeur]) break; // le mur n'existe pas
      if(laby.cell[c+l*laby.largeur]==laby.cell[(c+1)+l*laby.largeur]) break; // déjà connecté
      oid = laby.cell[(c+1)+l*laby.largeur];
      nid = laby.cell[c+l*laby.largeur];
      laby.mursV[(c+1)+l*laby.largeur]=false;
      trouve=true;
      break;
    case 2:// suppression d'un mur gauche
      if(c==0) break;// colonne invalide
      if(!laby.mursV[c+l*laby.largeur]) break; // le mur n'existe pas
      if(laby.cell[c+l*laby.largeur]==laby.cell[(c-1)+l*laby.largeur]) break; // déjà connecté
      oid = laby.cell[(c-1)+l*laby.largeur];
      nid = laby.cell[c+l*laby.largeur];
      laby.mursV[c+l*laby.largeur]=false;
      trouve=true;
      break;
    case 1:// suppression d'un mur haut
      if(l==0) break;// ligne invalide
      if(!laby.mursH[c+l*laby.largeur]) break; // le mur n'existe pas
      if(laby.cell[c+l*laby.largeur]==laby.cell[c+(l-1)*laby.largeur]) break; // déjà connecté
      oid = laby.cell[c+(l-1)*laby.largeur];
      nid = laby.cell[c+l*laby.largeur];
      laby.mursH[c+l*laby.largeur]=false;
      trouve=true;
      break;
    case 3:// suppression d'un mur bas
      if(l==laby.hauteur-1) break;// ligne invalide
      if(!laby.mursH[c+(l+1)*laby.largeur]) break; // le mur n'existe pas
      if(laby.cell[c+l*laby.largeur]==laby.cell[c+(l+1)*laby.largeur]) break; // déjà connecté
      oid = laby.cell[c+(l+1)*laby.largeur];
      nid = laby.cell[c+l*laby.largeur];
      laby.mursH[c+(l+1)*laby.largeur]=false;
      trouve=true;
      break;   
    }//switch
    
    if(!trouve) continue;

    // un mur a été supprimé - maj des id des cellules voisines
    // version basique de la màj par parcours de toutes les
    // cellules du labyrinthe
    nb++;
    for(int i=0; i<laby.largeur; i++)
      for(int j=0; j<laby.hauteur; j++)
	if(laby.cell[i+j*laby.largeur]==oid) laby.cell[i+j*laby.largeur]=nid;
  }
}

void dessinerLabyrinthe(labyrinthe &laby, const string &nomFichier){
  ofstream out;

  if(!ouvrirFichierSVG(nomFichier, out,
		       laby.largeur*CELLSIZE+2*MARGE,
		       laby.hauteur*CELLSIZE+2*MARGE))
    return;

  // dessin du fond
  rect(out, 0, 0,
       laby.largeur*CELLSIZE+2*MARGE,
       laby.hauteur*CELLSIZE+2*MARGE, "white");

   // dessin des murs verticaux
  for(int c=0; c<laby.largeur+1; c++)
    for(int l=0; l<laby.hauteur; l++)
      if(laby.mursV[c+l*laby.largeur]){// dessiner un mur vertical
	ligne(out,
	      c*CELLSIZE+MARGE, l*CELLSIZE+MARGE,
	      c*CELLSIZE+MARGE, (l+1)*CELLSIZE+MARGE,
	      "black", 3);
      }
  
  // dessin des murs horizontaux
  for(int c=0; c<laby.largeur; c++)
    for(int l=0; l<laby.hauteur+1; l++)
      if(laby.mursH[c+l*laby.largeur]){// dessiner un mur horizontal
	ligne(out,
	      c*CELLSIZE+MARGE, l*CELLSIZE+MARGE,
	      (c+1)*CELLSIZE+MARGE, l*CELLSIZE+MARGE,
	      "black", 3);
      }
  fermerFichierSVG(out);
}

// --------------------------------------------------------------------
// définition des fonctions liées à l'écriture dans les fichiers SVG
// --------------------------------------------------------------------

bool ouvrirFichierSVG(const string &nomFichier, ofstream &out,
		      int largeur, int hauteur){
  out.open(nomFichier);
  
  if(!out.is_open()){
    cout << "erreur d'ouverture du fichier de dessin " << nomFichier << endl;
    return false;
  }

  // sortie de l'entête
  out << "<?xml version=\"1.0\" encoding=\"utf-8\"?>" << endl;
  out << "<svg xmlns=\"http://www.w3.org/2000/svg\" version=\"1.1\" ";
  out <<"width=\"" << largeur << "\" height=\"" << hauteur << "\">" << endl;
  
  return true;
}


void fermerFichierSVG(ofstream &out){
  // fermeture de la balise svg
  out << "</svg>" << endl;

  out.close();
}

void text(ofstream &out, int x, int y, int size, const string &txt, const string &color){

  out << "<text x=\"" << x << "\" y=\"" << y << "\"" ;
  out << " font-family=\"Verdana\" font-size=\"" << size << "\"";
  out << " text-anchor=\"middle\" " ;
  out << " dominant-baseline=\"middle\" ";
  out << "fill=\"" << color << "\" >" << endl;
  out << txt << endl; 
  out << "</text>" << endl;
}

void ligne(ofstream &out, int x1, int y1, int x2, int y2, const string& color, int width){
  out <<  "<line x1=\"" << x1<< "\" y1=\"" << y1 << "\"";
  out << " x2=\"" << x2<< "\" y2=\"" << y2;
  out << "\" stroke=\"" << color << "\"";
  out << " stroke-width=\"" << width << "\"" << " />" << endl;
}

void rect(ofstream &out, int x, int y, int width, int height,  const string &color){
  out << "<rect width=\"" << width << "\" height=\"" << height << "\"";
  out << " x=\"" << x << "\" y=\"" << y << "\" fill=\"" << color << "\"";
  out << " />" << endl;
}

// --------------------------------------------------------------------
// Fonction de conversion d'une valeur entière en chaîne de caractères
// --------------------------------------------------------------------

string intToString(int v){
  stringstream s;
  s << v;
  return s.str();
}
  
